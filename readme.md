# QA Codetest

Hello dear QA candidate :)

If you are reading this you probably have thought about becoming part of our small but awesome team ... So good luck with the test!

## Conditions

There's no time limits to complete the test. We're not going to add any hidden code to monitor your first test server request to check when you start. We're coders too and we have made code tests too, and we know how it works and how it feel. So take your time, seriously it's ok if you want to take a whole weekend to complete the test!

This code test is designed to take no more than a couple of hours. However you can improve the code as much as you want. As more improvements you add (or  propose) the happier we'll be :D. 

Of course if you send us the solution after just 30 minutes we'll be impressed as hell and not so impressed if you answer in a week, but please keep in mind that we prefer a nice solution that took you a day than a dirty one that took you an hour.


## The problem

We have developed a simple message sender platform but we don't feel confortable with the code. **So we need to refactor the code** in order to make it more maintainable.
You can change the internals as mach as you want but please, don't change our "lovely CLI interface".
However you may take into account we'll add new message senders and new interfaces to call the system.


With this code we find the way you can detect the messy and undesirable code, measure it and give a way to fix those potential problems.
We'll take into account the **POO** principles applied, the **readability** of the new code and **maintainability**

## What we want

We want you to **refactor** the code in order to get:

* Object Oriented code
* Unit tests
* Find the main bad smells and fix them

## Bonus points

* Implement functional test
* Provide the metrics to know how good is our code (what metrics are important for you and how you get it)
* You find the way we can see the steps you followed to get the awesome code (tip: git could be a good friend here :P)
* Provides a way to externally configure the email account and the sms gateway
* Anything could make the project better is welcomed
* Send us feedback about this codetest (we also want to improve the recruitment process)



