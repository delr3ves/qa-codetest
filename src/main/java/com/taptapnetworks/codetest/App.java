package com.taptapnetworks.codetest;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.net.URLEncoder;
import java.util.Properties;

/**
 * Message Sender!
 */
public class App {
    public static void main(String[] args) {
        if (args.length < 4) {
            System.out.println("Ussage: codetest <type> <message> <to> <subject>");
            return;
        }
        try {
            String type = args[0];
            String message = args[1];
            String to = args[2];
            String subject = args[3];

            if (type.equals("SMS")) {
                HttpClient pasarelaSMS = new DefaultHttpClient();
                HttpGet request = new HttpGet("http://mySMSGateway.com?" + URLEncoder.encode(
                        "message=" + message + "&to=" + to));
                pasarelaSMS.execute(request);
            } else if (type.equals("mail")) {
                Properties p = new Properties();
                p.put("mail.smtp.auth", "true");
                p.put("mail.smtp.starttls.enable", "true");
                p.put("mail.smtp.host", "smtp.gmail.com");
                p.put("mail.smtp.port", "587");

                Session session = Session.getInstance(p,
                        new javax.mail.Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication("myemail@gmail.com", "thePassword");
                            }
                        });
                MimeMessage mm = new MimeMessage(session);
                // Set From: header field of the header.
                mm.setFrom(new InternetAddress("theUsername@email.com"));
                // Set To: header field of the header.
                mm.addRecipient(Message.RecipientType.TO,
                        new InternetAddress(to));
                // Set Subject: header field
                mm.setSubject(subject);
                // Now set the actual message
                mm.setText(message);
                // Send message
                Transport.send(mm);
            } else {
                throw new RuntimeException("Type is unknown");
            }
            System.out.println("Mensaje enviado");
        } catch (Exception e) {
            System.out.println("Error al enviar mensaje");
        }
    }
}
